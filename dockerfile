FROM openjdk:17

WORKDIR /app

COPY target/book-service-0.0.1-SNAPSHOT.jar /app/book-service-0.0.1.jar

EXPOSE 8081

CMD ["java", "-jar", "book-service-0.0.1.jar"]