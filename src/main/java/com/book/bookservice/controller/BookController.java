package com.book.bookservice.controller;

import com.book.bookservice.model.BaseResponse;
import com.book.bookservice.model.Book;
import com.book.bookservice.service.BookService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/books")
public class BookController {
    private static final Logger log = LoggerFactory.getLogger(BookController.class);
    private final BookService bookService;

    public BookController(BookService bookService) {
        this.bookService = bookService;
    }

    @GetMapping
    public ResponseEntity<BaseResponse<List<Book>>> getAllBooks() {
        List<Book> books = bookService.getAllBooks();
        BaseResponse<List<Book>> response = new BaseResponse<>("ok", HttpStatus.OK.value(), books,"");
        log.info("Data response get all books {} ", response);
        return ResponseEntity.ok().body(response);
    }

    @GetMapping("/{id}")
    public ResponseEntity<BaseResponse<Book>> getBookById(@PathVariable Long id) {
        Book book = bookService.getBookById(id);
        BaseResponse<Book> response = new BaseResponse<>("ok", HttpStatus.OK.value(), book,"");
        log.info("Data response getBookById {} ", response);

        return ResponseEntity.ok().body(response);
    }

    @PostMapping
    public ResponseEntity<BaseResponse<Void>> createBook(@RequestBody Book book) {
        bookService.createBook(book);
        BaseResponse<Void> response = new BaseResponse<>("ok", HttpStatus.OK.value(), null,"");
        log.info("Data response createBook {} ", response);
        return ResponseEntity.status(HttpStatus.OK).body(response);

    }

    @PostMapping("/{id}")
    public ResponseEntity<BaseResponse<List<Book>>> updateBook(@PathVariable Long id, @RequestBody Book book) {
        BaseResponse<List<Book>> response;
        try{
            book.setId(id);
            bookService.updateBook(book);
            List<Book> bookData = bookService.getAllBooks();
            response = new BaseResponse<List<Book>>("ok", HttpStatus.OK.value(), bookData,"Success Update Book");

            log.info("Data response updateBook {} ", response);

            return ResponseEntity.ok().body(response);
        }catch (Exception e){
            response = new BaseResponse<>("error", HttpStatus.BAD_REQUEST.value(), null, "Book doesn't Exists");

            log.error("Data response updateBook {} ", response);

            return ResponseEntity.badRequest().build();

        }


    }

    @DeleteMapping("/{id}")
    public ResponseEntity<BaseResponse<List<Book>>> deleteBook(@PathVariable Long id) {
        BaseResponse<List<Book>> response;

        try{
            bookService.deleteBook(id);
            List<Book> bookData = bookService.getAllBooks();
            response = new BaseResponse<>("ok", HttpStatus.OK.value(), bookData,"Success Delete Book");
            log.info("Data response deleteBook {} ", response);

            return ResponseEntity.ok().body(response);
        }catch (Exception e){
            bookService.deleteBook(id);
            response = new BaseResponse<>("error", HttpStatus.OK.value(), null,"Failed to Delete Book");
            log.error("Data response updateBook {} ", response);

            return ResponseEntity.ok().body(response);
        }

    }
}
