package com.book.bookservice.mapper;

import com.book.bookservice.model.Book;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface BookMapper {
    @Select("SELECT * FROM book")
    List<Book> getAllBooks();

    @Select("SELECT * FROM book WHERE id = #{id}")
    Book getBookById(Long id);

    @Insert("INSERT INTO book(title, author) VALUES(#{title}, #{author})")
    @Options(useGeneratedKeys = true, keyProperty = "id")
    void insertBook(Book book);

    @Update("UPDATE book SET title = #{title}, author = #{author} WHERE id = #{id}")
    void updateBook(Book book);

    @Delete("DELETE FROM book WHERE id = #{id}")
    void deleteBook(Long id);
}