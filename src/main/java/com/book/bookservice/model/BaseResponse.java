package com.book.bookservice.model;

import lombok.Data;

@Data
public class BaseResponse<T> {
    private String status;
    private int responseCode;
    private T data;
    private String message;

    public BaseResponse(String status, int responseCode, T data, String message) {
        this.status = status;
        this.responseCode = responseCode;
        this.data = data;
        this.message = message;
    }
}