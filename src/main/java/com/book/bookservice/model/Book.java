package com.book.bookservice.model;

import lombok.Data;

@Data
public class Book {
    private Long id;
    private String title;
    private String author;
}
