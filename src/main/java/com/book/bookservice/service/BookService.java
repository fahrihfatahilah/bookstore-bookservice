package com.book.bookservice.service;

// BookService.java
import com.book.bookservice.controller.BookController;
import com.book.bookservice.mapper.BookMapper;
import com.book.bookservice.model.Book;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookService {
    private static final Logger log = LoggerFactory.getLogger(BookService.class);

    private final BookMapper bookMapper;

    @Autowired
    public BookService(BookMapper bookMapper) {
        this.bookMapper = bookMapper;
    }

    public List<Book> getAllBooks() {
        return bookMapper.getAllBooks();
    }

    public Book getBookById(Long id) {
        return bookMapper.getBookById(id);
    }

    public void createBook(Book book) {
        bookMapper.insertBook(book);
    }

    public void updateBook(Book book) {
        Book getBookbyId = bookMapper.getBookById(book.getId());
        System.out.println("data book"+getBookbyId.getId());
        if(getBookbyId.getId()!=null){
            bookMapper.updateBook(book);
        }


    }

    public void deleteBook(Long id) {
        bookMapper.deleteBook(id);
    }
}
